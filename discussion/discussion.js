// MongoDB - query operators and field projection

// Inserting data through our database
db.users.insertMany([
    {
        firstName: "Jane",
        lastName: "Doe",
        age: 21,
        contact: {
            phone: "87654321",
            email: "janedoe@gmail.com"
        },
        courses: [ "CSS", "Javascript", "Python" ],
        department: "none"
    },
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 50,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "none"
    }
]);

//COMPARISON OPERATOR QUERY OPERATORS

// $gt/$gte operator 
// allows us to find documents that have field number values greater than or equal to a specified value.

db.users.find({ age : { $gt : 50 }   });//$gt = greater than
db.users.find({ age : { $gte : 50 }  });//$gte = greater than or equal

// $lt/lte operator
// allows us to find documents that have field number values less than or equal to a specified value.

db.users.find({ age : { $lt : 50 }   });//$lt = less than
db.users.find({ age : { $lte : 50 }  });//$lte = less than or equal

// $ne operator 
// allows us to find documents that have field number values not equal to a specified value.

db.users.find({ age : { $ne : 82 }   });//$ne = not equal

//$in operator
/*
    - Allows us to find documents with specific match criteria one field using different values
    - SYNTAX
        db.collectionName.find( { field : { $in : value } } )
note: CASE SENSITIVE!
*/
db.users.find( { lastName: { $in: [ "Hawking", "Doe"] } } );
db.users.find( { courses: { $in: [ "HTML", "React" ]  }  }  );


// LOGICAL QUERY OPERATORS
//$or operator

db.users.find( {$or: [ { firstName: "Neil" }, { age: 25 } ]   }  );// finds with the name "Neil" or age equals 25
// find multiple conditions

db.users.find( {$or: [ { firstName: "Neil" }, { age: { $gt: 30 } } ]   }  ); // finds with the name "Neil" or age greater than 30


// $and OPERATOR

/*
    - Allows us to find documents matching multiple criteria in a single field.
*/

db.users.find( { $and: [    {age:{ $ne: 82}},  {age: {$ne: 50}}     ] } );

//  [SECTION] FIELD PROJECTION
/*
    - Retrieving documents are common operators that we do and by default MongoDB queries return the whole document as a response
    - When dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.
    - To help with the readability od the values returned, we can include/exclude fields from the response.
*/

//  INCLUSION
/*  
    - Allows us to include/add specific fields only when retrieving documents.
    - The value provided is 1 or true to denote that the field is being included
    - SYNTAX
        db.users.find({citeria}, {field: 1})

*/

db.users.find(
        {
            firstName: "Jane"

        },
        {
            firstName: 1, // or firstName: true,    // to view the specific data you want
            lastName: 1, // or lastName: true,
            contact: 1

            _id: 0 // exemption para msabay ang inclusion and exclusion  "_id"
            // if you want age to view, then[view: 1]
        }

);


db.users.find(
    {
        firstName: "Stephen"

    },
    {
        firstName: 1,
        lastName: 1,
        "contact.phone": 1 // folder inside contact,, to view specific subfolder
       
    }

);








//  EXCLUSION
/*
- Allows us to EXCLUDE/REMOVE specific fields only when retrieving documents.
    - The value provided is 0 or false to denote that the field is being included
    - SYNTAX
        db.users.find({citeria}, {field: 0 OR FALSE})


*/

db.users.find(
    {
        firstName: "Stephen"

    },
    {
      department: 0,  // or firstName: false,    // to remove the specific data you want
         
         contact: 0
         
       
    }

);



//  EVALUATION QUERY OPERATORS

// $regex operator

/*
    - Allows us to find documents that match a specific string patter using regular expressions.
    - $options: '$i' - is for Case insensitivity to match upper and lower cases

    -$options: '$x' - "Extended" capability to ignore all white space characters in the $regex pattern unless escaped or included in a character class
*/

// Case sensitive query
db.users.find(     {firstName:    {$regex: 'N'} }) // all data with "N" in the firstName





// Case insensitive query
db.users.find(      {firstName:    {$regex: 'j', $options} }  )

//      - $options: '$i' - is for Case insensitivity to match upper and lower cases
db.users.find(      {firstName:    {$regex: 'j', $options: '$i'} }  )



db.users.find(      {firstName:    {$regex: 'j', $options: '$i'} }  )






// updating name for $x practice with whitespace
db.users.updateOne(
    {
        firstName: Jane
    },
    
    {   
        $set: {
            firstName: "Jane Rose"
        }
    }
)

//      - $options: '$x' - "Extended" capability to ignore all white space characters in the $regex pattern unless escaped or included in a character class


db.users.find(      {firstName:    {$regex: 'o', $options: '$x'} }  )// pwede na wala,,, updated an ang system lol